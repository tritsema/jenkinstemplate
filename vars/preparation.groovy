def call(Map config) {
  /*
    This routine will checkout the GIT_URL found in env and will add the entry credentializedScmUrl to the env.
    That entry can be used to commits changes made during the build process.
  */
  node {
    config.each { entry ->
      println "Name: $entry.key Value: $entry.value"
    }

    // checkout the code
    git branch: config.BRANCH_NAME, credentialsId: config.bitbucketCredentialsId, url: config.GIT_URL

    // fetch username and password from credentials, urlencode them and store them in env
    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: config.bitbucketCredentialsId, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
      script {
        env.username = env.USERNAME
        def urlencoded_username = java.net.URLEncoder.encode(env.USERNAME, "UTF-8")
        def urlencoded_password = java.net.URLEncoder.encode(env.PASSWORD, "UTF-8")
        env.credentializedScmUrl = config.GIT_URL.replace("//", "//${urlencoded_username}:${urlencoded_password}@")
      }
    }
  }
}